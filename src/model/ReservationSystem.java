package model;

import javax.swing.*;
import java.sql.*;

public class ReservationSystem {

    private Connection con;

    public ReservationSystem(Connection con) {
        this.con = con;

    }

    public boolean reserveEquipment(int cardholderID, int equipmentID, Timestamp date) {
        String rentLogQuery = "select equipmentid from equipmentrentlog e where e.equipmentid = ? and e.edate = ?";
        ResultSet reservedEquipmentLog;
        try {
            PreparedStatement preparedStatement = con.prepareStatement(rentLogQuery);
            preparedStatement.setInt(1, equipmentID);
            preparedStatement.setTimestamp(2, date);
            preparedStatement.execute();

            reservedEquipmentLog = preparedStatement.getResultSet();
            int eID = 0;
            while (reservedEquipmentLog.next()) {
                eID = reservedEquipmentLog.getInt("equipmentid");
            }

            preparedStatement.close();

            if (eID == 0) {
                String sql = "insert into equipmentrentlog (id, edate, equipmentid) values(?,?,?)";
                preparedStatement = con.prepareStatement(sql);
                preparedStatement.setInt(1, cardholderID);
                preparedStatement.setTimestamp(2, date);
                preparedStatement.setInt(3, equipmentID);
                preparedStatement.executeUpdate();
                preparedStatement.close();
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                    ex.getMessage(),
                    "SQL Exception",
                    JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }

    public boolean reserveRoom(int cardholderID, int roomNumber, Timestamp date) {
        String reservedRoomQuery = "select roomnumber from roomreservation r where r.roomnumber = ? and r.rdate = ?";
        ResultSet reservedRooms;
        try {
            PreparedStatement preparedStatement = con.prepareStatement(reservedRoomQuery);
            preparedStatement.setInt(1, roomNumber);
            preparedStatement.setTimestamp(2, date);
            preparedStatement.execute();

            reservedRooms = preparedStatement.getResultSet();
            int rNo = 0;
            while (reservedRooms.next()) {
                rNo = reservedRooms.getInt("roomnumber");
            }

            preparedStatement.close();

            if (rNo == 0) {

                String sql = "insert into roomreservation (rdate, roomnumber, id) values(?,?,?)";
                preparedStatement = con.prepareStatement(sql);
                preparedStatement.setTimestamp(1, date);
                preparedStatement.setInt(2, roomNumber);
                preparedStatement.setInt(3, cardholderID);
                preparedStatement.executeUpdate();
                preparedStatement.close();
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                    ex.getMessage(),
                    "SQL Exception",
                    JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }

    public void deleteReservation(int cardholderID, int roomnNumber, Timestamp date) {

        String sql;
        try {
            sql = "DELETE FROM roomreservation where id=? and roomNumber=? and rdate=?";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setInt(1,cardholderID);
            preparedStatement.setInt(2,roomnNumber);
            preparedStatement.setTimestamp(3,date);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {

            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                    e.getMessage(),
                    "SQL Exception",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}






