package model;

import javax.swing.*;
import java.sql.*;


public class
        EventSystem {

    private Connection con;

    public EventSystem(Connection con) {
        this.con = con;
    }

    public boolean scheduleEvent(Timestamp startTime, Timestamp endTime, String name, String description) {
        try {
            String sql = "insert into EVENTS (startTime, endTime, name, description) values (?,?,?,?)";
            PreparedStatement preparedStatement;
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setTimestamp(1, startTime);
            preparedStatement.setTimestamp(2, endTime);
            preparedStatement.setString(3, name);
            preparedStatement.setString(4, description);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                    e.getMessage(),
                    "SQL Exception",
                    JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }

    public boolean attendEvent(int cardholderID, String eventName, Timestamp startTime) {
        try {
            String sql = "insert into ATTENDS (ID, Name, StartTime) values (?,?,?)";
            PreparedStatement preparedStatement;
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setInt(1, cardholderID);
            preparedStatement.setString(2, eventName);
            preparedStatement.setTimestamp(3, startTime);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                    e.getMessage(),
                    "SQL Exception",
                    JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }

    public boolean staffEvent(int employeeID, String eventName, Timestamp startTime) {
        try {
            String sql = "insert into STAFFS (ID, StartTime, Name) values(?,?,?)";
            PreparedStatement preparedStatement;
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setInt(1, employeeID);
            preparedStatement.setTimestamp(2, startTime);
            preparedStatement.setString(3, eventName);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                    e.getMessage(),
                    "SQL Exception",
                    JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }


    public ResultSet selectCardholder(String[] events) throws SQLException {

        String sql;

        int length = events.length;
        ResultSet resultSet = null;

        try {
            sql = "create view target(name) as (Select name from attends where name in ";
            String event = "(";
            for (int i = 0; i < length - 1; i++) {
                event += "'" + events[i] + "',";
            }
            event += "'" + events[length - 1] + "')";
            sql += event + ")";
            System.out.println(sql);
            Statement statement = con.createStatement();
            statement.executeQuery(sql);

            sql = "create view temp(id) as ( ";
            sql += "Select a.id From attends a where NOT exists( " +
                    " Select t.name from target t where Not exists (" +
                    " Select a2.id from attends a2 where" +
                    " a.id=a2.id AND " +
                    " a2.name=t.name" +
                    ")" +
                    ")";
            sql += ")";
            statement.executeQuery(sql);
            sql = "Select distinct c.id, c.name From cardholder c, temp t where c.id=t.id";
            resultSet = statement.executeQuery(sql);

            Statement statement2 = con.createStatement();

            statement2.execute("drop view target");
            statement2.execute("drop view temp");
        } catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                    e.getMessage(),
                    "SQL Exception",
                    JOptionPane.ERROR_MESSAGE);
        }
        return resultSet;
    }

}
