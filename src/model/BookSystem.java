package model;

import javax.swing.*;
import java.sql.*;

/**
 * Created by douglas on 16/6/12.
 */
public class BookSystem {

    private LibraryDB libraryDB;

    public BookSystem(LibraryDB libraryDB) {
        this.libraryDB = libraryDB;
    }

    public boolean checkout(int cardholderID, int trackingNumber) throws SQLException {

        Statement stmt;
        ResultSet checkoutLog;

        try {

            double fee = libraryDB.getFeeSystem().getCardholderFees(cardholderID);

            // this part may need to modified by checkfee() function; here I use fee>0 to indicate fee unpaid for now
            if (fee > 0) {
                JFrame frame = new JFrame("ErrorDialog");
                JOptionPane.showMessageDialog(frame,
                                              "Unable to check book out:" +
                                              "Customer has unpaid fees.",
                                              "SQL Exception",
                                              JOptionPane.ERROR_MESSAGE);
                return false;
            }
            else if (trackingNumber == 0) {
                JFrame frame = new JFrame("ErrorDialog");
                JOptionPane.showMessageDialog(frame,
                                              "Unable to check book out:" +
                                              "No copies available.",
                                              "SQL Exception",
                                              JOptionPane.ERROR_MESSAGE);
                return false;
            }
            else {


                // create a new tuple in the checkoutLog table

                int checkoutID = nextID("CHECKOUTLOG");

                PreparedStatement preparedStatement;
//                String sql = "insert into CheckOutLog (id, cDate) values(?,?)";
//                preparedStatement = libraryDB.getCon().prepareStatement(sql);
//
////                java.sql.Date date = new java.sql.Date(System.currentTimeMillis()); // using the current time as check out time
////                Timestamp cDate = new Timestamp(date.getTime());
//                Timestamp cDate = getCurrentTime();
//                preparedStatement.setInt(1, checkoutID);
//                preparedStatement.setTimestamp(2, cDate);
//                preparedStatement.executeUpdate();
//                preparedStatement.close();

                // create a new tuple in the checksOut table
                String sql = "insert into checksOut (id, cardholderId,trackingNumber) values(?,?,?)";
                preparedStatement = libraryDB.getCon().prepareStatement(sql);
                preparedStatement.setInt(1, checkoutID);
                preparedStatement.setInt(2, cardholderID);
                preparedStatement.setInt(3, trackingNumber);
                preparedStatement.executeUpdate();
                preparedStatement.close();

            }
        }
        catch (SQLException ex) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          ex.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }
        return true;
    }

    public int findAvailableBook(String isbn) {

        //find one of the book with given ISBN that has not been checked out and return its trackingNumber

        int trackingNumber = 0;
        try {
            PreparedStatement stmt;
            ResultSet rsBook;

            stmt = libraryDB.getCon().prepareStatement("SELECT Min(b.trackingnumber) as availableTrackingNumber " +
                                                       "FROM  employeeQuery b " +
                                                       "WHERE b.isbn=? and " +
                                                       "b.trackingNumber NOT in " +
                                                       "(SELECT c.trackingNumber " +
                                                       "From checksOut c" +
                                                       ")");
            stmt.setString(1, isbn);
            rsBook = stmt.executeQuery();
            rsBook.next();
            trackingNumber = rsBook.getInt("availableTrackingNumber");
        }
        catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }
        return trackingNumber;

    }

    public int findAvailableBook(String title, String author) {

        //find one of the book with given title and author that has not been checked out and return its trackingNumber

        int trackingNumber = 0;
        try {
            ResultSet rsBook;
            String sql;
            PreparedStatement preparedStatement;
            sql = "SELECT Min(b.trackingNumber) as availableTrackingNumber " +
                  "FROM userQuery u, employeeQuery b " +
                  "WHERE UPPER(u.title) = ?" + " and " +
                  "UPPER(u.author) = ?" + " and " +
                  "b.ISBN=u.ISBN" + " and " +
                  "b.trackingNumber NOT in" +
                  "(SELECT c.trackingNumber " +
                  "From checksOut c" +
                  ")";
            title = title.toUpperCase();
            author = author.toUpperCase();
            preparedStatement = libraryDB.getCon().prepareStatement(sql);
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, author);
            rsBook = preparedStatement.executeQuery();
            rsBook.next();
            trackingNumber = rsBook.getInt("availableTrackingNumber");
        }
        catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }
        return trackingNumber;

    }

    public void addBook(String isbn, String title, String author, String genre, double price) {

        //add book information to userQuery, employeeQuery, books table

        try {

            String sql = "insert into books (ISBN,title, author, genre, price) values(?,?,?,?,?)";
            PreparedStatement preparedStatement;
            preparedStatement = libraryDB.getCon().prepareStatement(sql);
            preparedStatement.setString(1, isbn);
            preparedStatement.setString(2, title);
            preparedStatement.setString(3, author);
            preparedStatement.setString(4, genre);
            preparedStatement.setDouble(5, price);
            preparedStatement.executeUpdate();

            sql = "insert into userQuery (title, author,ISBN) values(?,?,?)";
            preparedStatement = libraryDB.getCon().prepareStatement(sql);
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, author);
            preparedStatement.setString(3, isbn);
            preparedStatement.executeUpdate();

            sql = "insert into employeeQuery (trackingNumber, ISBN) values(?,?)";
            preparedStatement = libraryDB.getCon().prepareStatement(sql);
            int trackingNumber = nextTrackingNumber();
            preparedStatement.setInt(1, trackingNumber);
            preparedStatement.setString(2, isbn);
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }

    }

    public void addBookCopy(String isbn) {
        // add a copy of book with given isbn

        try {
            PreparedStatement checkISBNExists;
            String cie = "SELECT Count(*) " +
                         "FROM Books " +
                         "WHERE isbn = ?";
            checkISBNExists = libraryDB.getCon().prepareStatement(cie);
            checkISBNExists.setString(1, isbn);
            checkISBNExists.execute();
            ResultSet resultSet = checkISBNExists.getResultSet();
            boolean existsISBN = false;
            while (resultSet.next()) {
                if (resultSet.getInt(1) > 0) {
                    existsISBN = true;
                }
            }

            if (existsISBN) {
                PreparedStatement preparedStatement;
                String sql;
                sql = "insert into employeeQuery (trackingNumber, ISBN) values(?,?)";
                preparedStatement = libraryDB.getCon().prepareStatement(sql);
                int trackingNumber = nextTrackingNumber();
                preparedStatement.setInt(1, trackingNumber);
                preparedStatement.setString(2, isbn);
                preparedStatement.executeUpdate();
            }
            else {
                JFrame frame = new JFrame("ErrorDialog");
                JOptionPane.showMessageDialog(frame,
                                              "No books with the given ISBN exist in the database.\n" +
                                              "Please use \"Add New Book to Library\".",
                                              "SQL Exception",
                                              JOptionPane.ERROR_MESSAGE);
            }

        }
        catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }
    }

    public void removeBook(int trackingNumber) {
        try {
            String sql;
            PreparedStatement preparedStatement;
            sql = "SELECT trackingNumber FROM CHECKSOUT WHERE trackingNumber=?";
            preparedStatement = libraryDB.getCon().prepareStatement(sql);
            preparedStatement.setInt(1, trackingNumber);
            ResultSet resultSet = preparedStatement.executeQuery();
            int result = 0;
            while (resultSet.next()) {
                result = resultSet.getInt("trackingNumber");
            }
            if (result == 0) {
                sql = "DELETE FROM employeeQuery Where trackingNumber=?";
                preparedStatement = libraryDB.getCon().prepareStatement(sql);
                preparedStatement.setInt(1, trackingNumber);
                preparedStatement.executeUpdate();
            }
            else {
                String s = "";
                JFrame frame = new JFrame("ErrorDialog");
                JOptionPane.showMessageDialog(frame,
                                              s,
                                              "Book already checked out",
                                              JOptionPane.ERROR_MESSAGE);
            }

        }
        catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }

    }

    public void removeSameBooks(String isbn) {
        String sql = "Select c.trackingNumber " +
                     "from checksout c, employeequery e " +
                     "where e.isbn = ? and e.trackingnumber = c.trackingnumber";
        try {
            PreparedStatement preparedStatement = libraryDB.getCon().prepareStatement(sql);
            preparedStatement.setString(1, isbn);
            ResultSet resultSet = preparedStatement.executeQuery();
            int trackingNumber = 0;
            while (resultSet.next()) {
                trackingNumber = resultSet.getInt("trackingNumber");
            }
            preparedStatement.close();
            if (trackingNumber == 0) {
                sql = "Delete From books where isbn=?";
                preparedStatement = libraryDB.getCon().prepareStatement(sql);
                preparedStatement.setString(1, isbn);
                preparedStatement.executeUpdate();
            }
            else {
                JFrame frame = new JFrame("ErrorDialog");
                JOptionPane.showMessageDialog(frame,
                                              "Remove fail: At least one book with given ISBN is checked out",
                                              "Removal Error",
                                              JOptionPane.ERROR_MESSAGE);
            }
        }
        catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }

    }

    public void returnBook(int trackingNumber, int cardholderID) {

        try {
            String sql = "Select trackingNumber FROM checksout where trackingNumber=? and cardholderID=?";
            PreparedStatement preparedStatement = libraryDB.getCon().prepareStatement(sql);
            preparedStatement.setInt(1, trackingNumber);
            preparedStatement.setInt(2, cardholderID);
            ResultSet resultSet = preparedStatement.executeQuery();
            int result = 0;
            while (resultSet.next()) {
                result = resultSet.getInt("trackingNumber");
            }
            if (result == 0) {

                String s = "";
                JFrame frame = new JFrame("ErrorDialog");
                JOptionPane.showMessageDialog(frame,
                                              "Book not checked out, or doesn't exist.",
                                              "Error",
                                              JOptionPane.INFORMATION_MESSAGE);

            }
            else {
                sql = "Select id FROM checksout where trackingNumber=? and cardholderID=?";
                preparedStatement = libraryDB.getCon().prepareStatement(sql);
                preparedStatement.setInt(1, trackingNumber);
                preparedStatement.setInt(2, cardholderID);
                resultSet = preparedStatement.executeQuery();
                int checkoutID = 0;
                while (resultSet.next()) {
                    checkoutID = resultSet.getInt("id");
                }

                sql = "Insert into returnLog (id, rDate) values(?,?)";
                preparedStatement = libraryDB.getCon().prepareStatement(sql);
                preparedStatement.setInt(1, checkoutID);
                preparedStatement.setTimestamp(2, getCurrentTime());
                preparedStatement.executeUpdate();

                sql = "DELETE FROM checksout Where id=?";
                preparedStatement = libraryDB.getCon().prepareStatement(sql);
                preparedStatement.setInt(1, checkoutID);
                preparedStatement.executeUpdate();

            }
        }
        catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }
    }

    public int nextTrackingNumber() {
        int trackingNumber = 0;
        try {

            ResultSet resultSet;
            Statement stmt = libraryDB.getCon().createStatement();

            resultSet = stmt.executeQuery("SELECT MAX(trackingNumber) as nextTrackingNumber " +
                                          "FROM employeeQuery"
                                         );
            while (resultSet.next()) {
                trackingNumber = resultSet.getInt("nextTrackingNumber");
                trackingNumber++;
            }
        }
        catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }
        return trackingNumber;
    }

    public int nextID(String table) {

        //generate next ID to use when adding an new instance to a table
        //note some tables don't have id as an attribute; it doesn't check for that

        int nextID = 0;
        try {
            ResultSet resultSet;
            table = table.toUpperCase();
            if (table.equals("employeeQuery")) {
                nextID = nextTrackingNumber();
            }
            PreparedStatement preparedStatement = libraryDB.getCon().prepareStatement("SELECT MAX(id) as nextID " +
                                                                                      "FROM " + table);
            preparedStatement.executeQuery();
            resultSet = preparedStatement.getResultSet();

            while (resultSet.next()) {
                nextID = resultSet.getInt("nextID");
                nextID++;
            }
            preparedStatement.close();
        }
        catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }
        return nextID;
    }

    public Timestamp getCurrentTime() {
        java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
        Timestamp currentTime = new Timestamp(date.getTime());
        return currentTime;
    }

    public ResultSet filter(String target, String table, String conditon, String groupBy, String having) {
        ResultSet resultSet = null;
        try {
            String sql = "SELECT " + target + " FROM " + table;
            if (conditon != null) {
                sql += " Where " + conditon;
            }
            if (groupBy != null) {
                sql += " group by " + groupBy;
            }
            if (having != null) {
                sql += " Having " + having;
            }
            Statement statement = libraryDB.getCon().createStatement();
            resultSet = statement.executeQuery(sql);
        }
        catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);

        }
        return resultSet;
    }

    public ResultSet maxGenre() throws SQLException {
        ResultSet resultSet;
        String sql = "create view temp(genre, avgPrice) as (" +
                     "Select genre, AVG(price) as avgPrice from books group by genre)";
        PreparedStatement preparedStatement = libraryDB.getCon().prepareStatement(sql);
        preparedStatement.executeQuery();
        resultSet = filter("genre, t1.avgPrice", "temp t1", "t1.avgPrice >= All( select t2.avgPrice from temp t2)", null, null);
        Statement statement = libraryDB.getCon().createStatement();
        statement.executeUpdate("drop view temp");
        return resultSet;
    }

    public ResultSet minGenre() throws SQLException {
        ResultSet resultSet;
        String sql = "create view temp(genre, avgPrice) as (" +
                     "Select genre, AVG(price) as avgPrice from books group by genre)";
        PreparedStatement preparedStatement = libraryDB.getCon().prepareStatement(sql);
        preparedStatement.executeQuery();
        resultSet = filter("genre, t1.avgPrice", "temp t1", "t1.avgPrice <= All( select t2.avgPrice from temp t2)", null, null);
        preparedStatement.executeUpdate("drop view temp");
        return resultSet;
    }

    public ResultSet selectBook(double price) {
        return filter("*", "books", "price<" + price, null, null);
    }

    public ResultSet selectBook(String genre) {
        genre = genre.toUpperCase();
        return filter("*", "books", "UPPER(genre)='" + genre + "'", null, null);
    }

    public String selectBook() throws SQLException {
        ResultSet resultSet= filter("title","books b","b.price<=All(select b2.price from books b2)",null,null);
        String title="";
        while(resultSet.next()){
            title=resultSet.getString("title");
        }
        return  title;
    }


}
