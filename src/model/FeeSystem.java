package model;

import javax.swing.*;
import java.sql.*;

public class FeeSystem {

    private Connection con;

    public FeeSystem(Connection con) {
        this.con = con;
    }

    public boolean addFee(int cardholderID, double fee) {
        try {
            ResultSet cardholder;
            PreparedStatement preparedStatement = con.prepareStatement("SELECT fee " +
                                                                       "FROM Cardholder c " +
                                                                       "Where c.id = ?");
            preparedStatement.setInt(1, cardholderID);
            preparedStatement.execute();
            cardholder = preparedStatement.getResultSet();

            double oldFee;
            if (cardholder.next()) {
                oldFee = cardholder.getDouble("fee");
            }
            else {
                JFrame frame = new JFrame("ErrorDialog");
                JOptionPane.showMessageDialog(frame,
                                              "No such cardholder ID.",
                                              "SQL Exception",
                                              JOptionPane.ERROR_MESSAGE);
                return false;
            }

            preparedStatement.close();
            String sql = "UPDATE Cardholder set FEE=? where id = ?";
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setDouble(1, oldFee + fee);
            preparedStatement.setInt(2, cardholderID);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;
        }
        catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }

    public double getCardholderFees(int cardholderID) throws SQLException {
        ResultSet cardholder;

        PreparedStatement preparedStatement = con.prepareStatement("SELECT * " +
                                                                   "FROM Cardholder c " +
                                                                   "Where c.id = ?");

        preparedStatement.setInt(1, cardholderID);
        preparedStatement.execute();
        cardholder = preparedStatement.getResultSet();


        double fee;
        cardholder.next();
        fee = cardholder.getDouble("fee");
        preparedStatement.close();
        return fee;
    }


    public ResultSet getFeeDetails(int cardholderID) throws SQLException {

        Timestamp lateDate = new Timestamp(System.currentTimeMillis() - (/*30 * 24 * 60 * 60 * 1*/1));
//        Timestamp lateDate = getCurrentTime();

        double fee = getCardholderFees(cardholderID);

        ResultSet cardholderFeeDetails;
        try {
            String sql = "SELECT o.trackingNumber, b.title, b.author, l.cdate " +
                         "FROM Cardholder c, ChecksOut o, books b, employeeQuery e , checkoutlog l " +
                         "WHERE c.id=? and o.trackingNumber = e.trackingNumber and e.ISBN = b.ISBN and l.id = o.id and l.cdate <=?";
//            String sql = "Insert into checkoutlog(id, date) values (9090, ?)";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setInt(1, cardholderID);
            preparedStatement.setTimestamp(2, lateDate);  // today's date - numDays before considered late
//            preparedStatement.setTimestamp(1, lateDate);
            cardholderFeeDetails=preparedStatement.executeQuery();
            //cardholderFeeDetails = preparedStatement.getResultSet();
            // preparedStatement.close();   // can't close prepared statement or result set might be prone to leak

            return cardholderFeeDetails;
        }
        catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    public boolean late(Timestamp timestamp) {
        Timestamp currentTime = getCurrentTime();
        int a = currentTime.getMonth() - timestamp.getMonth();
        return a >= 1 || a < 0;
    }

    public Timestamp getCurrentTime() {
        java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
        Timestamp currentTime = new Timestamp(date.getTime());
        return currentTime;
    }

}












