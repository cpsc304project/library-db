package model;// We need to import the java.sql package to use JDBC

import ui.AllBooksWindow;
import ui.LoginWindow;
import ui.MainWindow;
import ui.UpcomingEventsWindow;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;

// for reading from the command line
// for the login window


public class LibraryDB {

    // command line reader
    private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    private Connection con;
    private BookSystem bookSystem;
    private EventSystem eventSystem;
    private FeeSystem feeSystem;
    private ReservationSystem reservationSystem;

    /*
     * constructs login window and loads JDBC driver
     */
    public LibraryDB() {

        // Set Look and Feel
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }

        // Load the Oracle JDBC driver
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        }
        catch (SQLException ex) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          ex.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }

        // Show Login Window
        JDialog loginWindow = new LoginWindow(this);
        loginWindow.setVisible(true);

        this.bookSystem = new BookSystem(this);
        this.eventSystem = new EventSystem(con);
        this.feeSystem = new FeeSystem(con);
        this.reservationSystem = new ReservationSystem(con);
    }

    public static void main(String args[]) {
        new LibraryDB();
    }

    public ReservationSystem getReservationSystem() {
        return reservationSystem;
    }

    public FeeSystem getFeeSystem() {
        return feeSystem;
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public EventSystem getEventSystem() {
        return eventSystem;
    }

    public MainWindow showAdminMenu() {
        MainWindow mainWindow = new MainWindow(this);
        mainWindow.setVisible(true);
        return mainWindow;
    }

    /*
     * displays simple text interface
     */
    public void showTextMenu() {
        int choice;
        boolean quit;

        quit = false;

        try {
            // disable auto commit mode
            con.setAutoCommit(false);

            while (!quit) {
                System.out.print("\n\nPlease choose one of the following: \n");
                System.out.print("1.  Show books\n");
                System.out.print("2.  Quit\n>> ");

                choice = Integer.parseInt(in.readLine());

                System.out.println(" ");

                switch (choice) {
                    case 1:
                        showBooks();
                        break;
                    case 2:
                        quit = true;
                }
            }

            con.close();
            in.close();
            System.out.println("\nGood Bye!\n\n");
            System.exit(0);
        }
        catch (IOException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "IO Exception",
                                          JOptionPane.ERROR_MESSAGE);

            try {
                con.close();
                System.exit(-1);
            }
            catch (SQLException ex) {
                JOptionPane.showMessageDialog(frame,
                                              ex.getMessage(),
                                              "SQL Exception",
                                              JOptionPane.ERROR_MESSAGE);
            }
        }
        catch (SQLException ex) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          ex.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }
    }


    public void getBooks() {
        try {
            Statement statement = con.createStatement();

            ResultSet rsBooks = statement.executeQuery("SELECT title, author, genre, price, b.isbn, COUNT(trackingnumber) " +
                                                       "FROM books b, employeequery e " +
                                                       "WHERE b.isbn = e.isbn " +
                                                       "GROUP BY b.isbn, title, author, genre, price");
            // TODO: COUNT returns the wrong number in the table, but the right number in the command line.......

            AllBooksWindow allBooksWindow = new AllBooksWindow(rsBooks);
            allBooksWindow.setVisible(true);

            statement.close();

        }
        catch (SQLException ex) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          ex.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }
    }

    // Lists all books in the library, with their tracking number, ISBN, title, author, genre, and price
    private void showBooks() {

        String author;
        String title;
        String genre;
        double price;
        int ISBN;
        int trackingNumber;
        Statement stmt;
        ResultSet rsBooks;

        try {
            stmt = con.createStatement();

            rsBooks = stmt.executeQuery("SELECT trackingnumber, b.isbn, title, author, genre, price " +
                                        "FROM books b, employeequery e " +
                                        "WHERE b.isbn = e.isbn");

            // get info on ResultSet
            ResultSetMetaData rsmdBooks = rsBooks.getMetaData();

            // get number of columns
            int numCols = rsmdBooks.getColumnCount();

            System.out.println();

            // display column names;
            for (int i = 0; i < numCols; i++) {
                // get column name and print it
                System.out.printf("%-20s", rsmdBooks.getColumnName(i + 1));
            }

            System.out.println();

            while (rsBooks.next()) {
                // for display purposes get everything from Oracle
                // as a string
                // simplified output formatting; truncation may occur

                trackingNumber = rsBooks.getInt("TRACKINGNUMBER");
                System.out.printf("%-20d", trackingNumber);

                ISBN = rsBooks.getInt("ISBN");
                System.out.printf("%-20d", ISBN);

                title = rsBooks.getString("TITLE");
                System.out.printf("%-20s", title);

                author = rsBooks.getString("AUTHOR");
                System.out.printf("%-20s", author);

                genre = rsBooks.getString("GENRE");
                System.out.printf("%-20s", genre);

                price = rsBooks.getDouble("PRICE");
                System.out.printf("%-20.2f", price);

                System.out.println();
            }

            // close the statement;
            // the ResultSet will also be closed
            stmt.close();

        }
        catch (SQLException ex) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          ex.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);
        }
    }

    public void getUpcomingEvents() {

        Statement statement;
        ResultSet events;

        try {

            statement = con.createStatement();
            events = statement.executeQuery("SELECT name, description, starttime, endtime " +
                                            "FROM events e " +
                                            "WHERE e.endtime > SYSTIMESTAMP " +
                                            "ORDER BY endtime");

            UpcomingEventsWindow upcomingEventsWindow = new UpcomingEventsWindow(events);
            upcomingEventsWindow.setVisible(true);
            statement.close();

        }
        catch (SQLException e) {
            // TODO: Popup window
            System.out.println("Error: " + e.getMessage());
        }

    }

    public void setSchema(String string) {
        try {
            Statement statement = con.createStatement();
            statement.execute("ALTER SESSION SET CURRENT_SCHEMA=" + string);
            statement.execute("alter session set nls_timestamp_format = 'YYYY-MM-DD HH:MI:SS.FF6'");
        }
        catch (SQLException e) {
            //TODO: Popup window
            System.out.println("Error: " + e.getMessage());
        }
    }

    public ResultSet filter(String target, String table, String condition, String groupBy, String having, String orderBy) {
        ResultSet resultSet = null;
        try {
            String sql = "SELECT " + target + " FROM " + table;
            if (condition != null) {
                sql += " Where " + condition;
            }
            if (groupBy != null) {
                sql += " group by " + groupBy;
            }
            if (having != null) {
                sql += " Having " + having;
            }
            if (orderBy != null) {
                sql += " Order By " + orderBy;
            }
            Statement statement = con.createStatement();
            resultSet = statement.executeQuery(sql);
            statement.close();
        }
        catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "SQL Exception",
                                          JOptionPane.ERROR_MESSAGE);

        }
        return resultSet;
    }

    public void showCardholderMenu() {
        MainWindow menu = showAdminMenu();
        menu.removeEmployeeTab();
    }

    public void showEmployeeMenu() {
        MainWindow menu = showAdminMenu();
        menu.removeCardholderTab();
    }

    public BookSystem getBookSystem() {
        return bookSystem;
    }
}

