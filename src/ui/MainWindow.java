package ui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import model.LibraryDB;
import util.WindowUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;

public class MainWindow extends JFrame {

    private LibraryDB libraryDB;

    private JPanel contentPane;
    private JTabbedPane tabbedPane;
    private JButton checkOutABookButton;
    private JButton reserveRoomButton;
    private JButton checkCardholderInToEventButton;
    private JButton checkFeesButton;
    private JButton reserveEquipmentButton;
    private JButton viewUpcomingEventsButton;
    private JButton removeBookFromLibraryButton;
    private JButton addFeeToCardholderButton;
    private JButton scheduleEventButton;
    private JButton checkCardholderSFeesButton;
    private JButton checkEmployeeInToEventButton;
    private JButton addNewBookToButton;
    private JButton addExistingBookToButton;
    private JButton viewAllBooksButton;
    private JButton returnBookForCardholderButton;
    private JButton removeAllCopiesOfButton;
    private JButton viewEventAttendeesButton;
    private JButton viewMostLeastExpensiveButton;
    private JButton changeEventEndTimeButton;
    private JButton selectBookByPriceButton;
    private JButton selectBookByGenreButton;
    private JButton getCheapestBookNameButton;

    public MainWindow(LibraryDB libraryDB) {
        this.libraryDB = libraryDB;
        setContentPane(contentPane);

        setTitle("LibraryDB");
        WindowUtils.centerAndPack(this);
        setResizable(false);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    libraryDB.getCon().close();
                    System.exit(0);
                }
                catch (SQLException ex) {
                    JFrame frame = new JFrame("ErrorDialog");
                    JOptionPane.showMessageDialog(frame,
                                                  ex.getMessage(),
                                                  "SQL Exception",
                                                  JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        viewUpcomingEventsButton.addActionListener(e -> {
            libraryDB.getUpcomingEvents();
        });


        addExistingBookToButton.addActionListener(e -> {
            AddBookCopyWindow addBookCopyWindow = new AddBookCopyWindow(libraryDB);
            addBookCopyWindow.setVisible(true);
        });

        viewAllBooksButton.addActionListener(e -> {
            libraryDB.getBooks();
        });

        addNewBookToButton.addActionListener(e -> {
            AddNewBookWindow addNewBookWindow = new AddNewBookWindow(libraryDB);
            addNewBookWindow.setVisible(true);
        });
        removeBookFromLibraryButton.addActionListener(e -> {
            RemoveBookWindow removeBookWindow = new RemoveBookWindow(libraryDB);
            removeBookWindow.setVisible(true);
        });
        returnBookForCardholderButton.addActionListener(e -> {
            ReturnBookWindow returnBookWindow = new ReturnBookWindow(libraryDB);
            returnBookWindow.setVisible(true);
        });
        checkOutABookButton.addActionListener(e -> {
            CheckoutWindow checkoutWindow = new CheckoutWindow(libraryDB);
            checkoutWindow.setVisible(true);
        });
        removeAllCopiesOfButton.addActionListener(e -> {
            RemoveAllBooks removeAllBooks = new RemoveAllBooks(libraryDB);
            removeAllBooks.setVisible(true);
        });
        scheduleEventButton.addActionListener(e -> {
            ScheduleEventWindow scheduleEventWindow = new ScheduleEventWindow(libraryDB);
            scheduleEventWindow.setVisible(true);
        });
        checkEmployeeInToEventButton.addActionListener(e -> {
            EmployeeCheckIn employeeCheckIn = new EmployeeCheckIn(libraryDB);
            employeeCheckIn.setVisible(true);
        });
        checkCardholderInToEventButton.addActionListener(e -> {
            CardholderCheckIn cardholderCheckIn = new CardholderCheckIn(libraryDB);
            cardholderCheckIn.setVisible(true);
        });
        checkCardholderSFeesButton.addActionListener(e -> showCheckFeeWindow());
        checkFeesButton.addActionListener(e -> showCheckFeeWindow());
        addFeeToCardholderButton.addActionListener(e -> {
            AddFeeWindow addFeeWindow = new AddFeeWindow(libraryDB);
            addFeeWindow.setVisible(true);
        });
        reserveRoomButton.addActionListener(e -> {
            ReserveRoomWindow reserveRoomWindow = new ReserveRoomWindow(libraryDB);
            reserveRoomWindow.setVisible(true);
        });
        reserveEquipmentButton.addActionListener(e -> {
            ReserveEquipmentWindow reserveEquipmentWindow = new ReserveEquipmentWindow(libraryDB);
            reserveEquipmentWindow.setVisible(true);
        });
        viewEventAttendeesButton.addActionListener(e -> {
            ViewEventAttendees viewEventAttendees = new ViewEventAttendees(libraryDB);
            viewEventAttendees.setVisible(true);
        });
        viewMostLeastExpensiveButton.addActionListener(e -> {
            SelectGenrePrice selectGenrePrice = new SelectGenrePrice(libraryDB);
            selectGenrePrice.setVisible(true);
        });
        selectBookByPriceButton.addActionListener(e -> {
            SelectBookByPrice selectBookByPrice = new SelectBookByPrice(libraryDB);
            selectBookByPrice.setVisible(true);
        });
        selectBookByGenreButton.addActionListener(e -> {
            SelectBookByGenre selectBookByGenre = new SelectBookByGenre(libraryDB);
            selectBookByGenre.setVisible(true);
        });
        getCheapestBookNameButton.addActionListener(e -> {
            MinimumPricedBook minimumPricedBook = new MinimumPricedBook(libraryDB);
            minimumPricedBook.setVisible(true);
        });
    }

    private void showCheckFeeWindow() {
        CheckFeeWindow checkFeeWindow = new CheckFeeWindow(libraryDB);
        checkFeeWindow.setVisible(true);
    }

    public void removeEmployeeTab() {
        tabbedPane.removeTabAt(2);
    }

    public void removeCardholderTab() {
        tabbedPane.removeTabAt(1);
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        contentPane = new JPanel();
        contentPane.setLayout(new GridLayoutManager(1, 1, new Insets(10, 10, 10, 10), -1, -1));
        tabbedPane = new JTabbedPane();
        contentPane.add(tabbedPane, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(200, 200), null, 0, false));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(7, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane.addTab("General", panel1);
        viewUpcomingEventsButton = new JButton();
        viewUpcomingEventsButton.setText("View Upcoming Events");
        panel1.add(viewUpcomingEventsButton, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        viewAllBooksButton = new JButton();
        viewAllBooksButton.setText("View All Books");
        panel1.add(viewAllBooksButton, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel1.add(spacer1, new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        viewMostLeastExpensiveButton = new JButton();
        viewMostLeastExpensiveButton.setText("View Most/Least Expensive Genres");
        panel1.add(viewMostLeastExpensiveButton, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        selectBookByPriceButton = new JButton();
        selectBookByPriceButton.setText("Select Book by Price");
        panel1.add(selectBookByPriceButton, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        selectBookByGenreButton = new JButton();
        selectBookByGenreButton.setText("Select Book by Genre");
        panel1.add(selectBookByGenreButton, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        getCheapestBookNameButton = new JButton();
        getCheapestBookNameButton.setText("Get Cheapest Book Name");
        panel1.add(getCheapestBookNameButton, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(6, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane.addTab("Cardholder", panel2);
        checkOutABookButton = new JButton();
        checkOutABookButton.setText("Check Out a Book");
        panel2.add(checkOutABookButton, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        checkFeesButton = new JButton();
        checkFeesButton.setText("Check Fees");
        panel2.add(checkFeesButton, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        checkCardholderInToEventButton = new JButton();
        checkCardholderInToEventButton.setText("Check In to Event");
        panel2.add(checkCardholderInToEventButton, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        reserveEquipmentButton = new JButton();
        reserveEquipmentButton.setText("Reserve Equipment");
        panel2.add(reserveEquipmentButton, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        reserveRoomButton = new JButton();
        reserveRoomButton.setText("Reserve Room");
        panel2.add(reserveRoomButton, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        panel2.add(spacer2, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(12, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane.addTab("Employee", panel3);
        removeBookFromLibraryButton = new JButton();
        removeBookFromLibraryButton.setText("Remove One Copy of a Book");
        panel3.add(removeBookFromLibraryButton, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(120, 23), null, 0, false));
        addFeeToCardholderButton = new JButton();
        addFeeToCardholderButton.setText("Modify Cardholder's Fee");
        panel3.add(addFeeToCardholderButton, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(120, 23), null, 0, false));
        scheduleEventButton = new JButton();
        scheduleEventButton.setText("Schedule Event");
        panel3.add(scheduleEventButton, new GridConstraints(7, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(120, 23), null, 0, false));
        checkCardholderSFeesButton = new JButton();
        checkCardholderSFeesButton.setText("Check Cardholder's Fees");
        panel3.add(checkCardholderSFeesButton, new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(120, 23), null, 0, false));
        checkEmployeeInToEventButton = new JButton();
        checkEmployeeInToEventButton.setText("Check In to Event");
        checkEmployeeInToEventButton.setToolTipText("Use this to clock in while staffing an event.");
        panel3.add(checkEmployeeInToEventButton, new GridConstraints(8, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(120, 23), null, 0, false));
        addNewBookToButton = new JButton();
        addNewBookToButton.setText("Add New Book to Library");
        addNewBookToButton.setToolTipText("If there are no copies of this book in the library yet, use this to add the first copy.");
        panel3.add(addNewBookToButton, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        addExistingBookToButton = new JButton();
        addExistingBookToButton.setText("Add a Copy of an Existing Book");
        addExistingBookToButton.setToolTipText("If there is already a book with the same ISBN in the library, use this to add another copy with a unique tracking number.");
        panel3.add(addExistingBookToButton, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer3 = new Spacer();
        panel3.add(spacer3, new GridConstraints(11, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        returnBookForCardholderButton = new JButton();
        returnBookForCardholderButton.setText("Return Book for Cardholder");
        panel3.add(returnBookForCardholderButton, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        removeAllCopiesOfButton = new JButton();
        removeAllCopiesOfButton.setText("Remove All Copies of a Book");
        panel3.add(removeAllCopiesOfButton, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        viewEventAttendeesButton = new JButton();
        viewEventAttendeesButton.setText("View Event Attendees");
        panel3.add(viewEventAttendeesButton, new GridConstraints(9, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        changeEventEndTimeButton = new JButton();
        changeEventEndTimeButton.setText("Change Event End Time");
        panel3.add(changeEventEndTimeButton, new GridConstraints(10, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /** @noinspection ALL */
    public JComponent $$$getRootComponent$$$() {
        return contentPane;
    }
}
