package ui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import model.LibraryDB;
import util.WindowUtils;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class SelectGenrePrice extends JDialog {

    private JTable table;
    private JButton selectMostExpensiveBooksButton;
    private JButton selectLeastExpensiveBooksButton;
    private JPanel contentPane;

    public SelectGenrePrice(LibraryDB libraryDB) {
        setContentPane(contentPane);
        setModal(true);
        setTitle("Select Genres by Max/Min priced book");
        WindowUtils.centerAndPack(this);
        setResizable(false);

        selectMostExpensiveBooksButton.addActionListener(e -> {
            try {
                ResultSet resultSet = libraryDB.getBookSystem().maxGenre();
                if (resultSet != null) {
                    DefaultTableModel model = populateTable(resultSet);
                    if (model != null) {
                        table.setModel(model);
                        TableColumnModel columnModel = table.getColumnModel();
                        columnModel.getColumn(0).setPreferredWidth(150);
                        columnModel.getColumn(1).setPreferredWidth(150);
                        WindowUtils.centerAndPack(this);
                    }
                    else {
                        JFrame frame = new JFrame("ErrorDialog");
                        JOptionPane.showMessageDialog(frame,
                                                      "Table not obtained",
                                                      "Error",
                                                      JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
            catch (SQLException sqle) {
                JFrame frame = new JFrame("ErrorDialog");
                JOptionPane.showMessageDialog(frame,
                                              sqle.getMessage(),
                                              "SQL Exception",
                                              JOptionPane.ERROR_MESSAGE);
            }
        });
        selectLeastExpensiveBooksButton.addActionListener(e -> {
            try {
                ResultSet resultSet = libraryDB.getBookSystem().minGenre();
                if (resultSet != null) {
                    DefaultTableModel model = populateTable(resultSet);
                    if (model != null) {
                        table.setModel(model);
                        TableColumnModel columnModel = table.getColumnModel();
                        columnModel.getColumn(0).setPreferredWidth(150);
                        columnModel.getColumn(1).setPreferredWidth(150);
                        WindowUtils.centerAndPack(this);
                    }
                    else {
                        JFrame frame = new JFrame("ErrorDialog");
                        JOptionPane.showMessageDialog(frame,
                                                      "Table not obtained",
                                                      "Error",
                                                      JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
            catch (SQLException sqle) {
                JFrame frame = new JFrame("ErrorDialog");
                JOptionPane.showMessageDialog(frame,
                                              sqle.getMessage(),
                                              "SQL Exception",
                                              JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    private DefaultTableModel populateTable(ResultSet rs) {
        ResultSetMetaData metaData;
        try {
            metaData = rs.getMetaData();
            Vector<String> columnNames = new Vector<>();
            int columnCount = metaData.getColumnCount();

            columnNames.add("Genre");
            columnNames.add("Average Price");

            // data of the table
            Vector<Vector<Object>> data = new Vector<>();
            while (rs.next()) {
                Vector<Object> vector = new Vector<>();
                for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                    vector.add(rs.getObject(columnIndex));
                }
                data.add(vector);
            }

            return new DefaultTableModel(data, columnNames) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
        }
        catch (SQLException e) {
            JFrame frame = new JFrame("ErrorDialog");
            JOptionPane.showMessageDialog(frame,
                                          e.getMessage(),
                                          "SQLException",
                                          JOptionPane.ERROR_MESSAGE);
            return null;
        }
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        contentPane = new JPanel();
        contentPane.setLayout(new GridLayoutManager(2, 2, new Insets(10, 10, 10, 10), -1, -1));
        final JScrollPane scrollPane1 = new JScrollPane();
        contentPane.add(scrollPane1, new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        table = new JTable();
        scrollPane1.setViewportView(table);
        selectMostExpensiveBooksButton = new JButton();
        selectMostExpensiveBooksButton.setText("Select Most Expensive Books by Genre");
        contentPane.add(selectMostExpensiveBooksButton, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        selectLeastExpensiveBooksButton = new JButton();
        selectLeastExpensiveBooksButton.setText("Select Least Expensive Books by Genre");
        contentPane.add(selectLeastExpensiveBooksButton, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /** @noinspection ALL */
    public JComponent $$$getRootComponent$$$() {
        return contentPane;
    }
}
