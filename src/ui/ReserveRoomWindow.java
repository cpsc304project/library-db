package ui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import javafx.util.converter.DateTimeStringConverter;
import model.LibraryDB;
import util.WindowUtils;

import javax.swing.*;
import java.awt.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class ReserveRoomWindow extends JDialog {

    private JPanel contentPane;
    private JTextField idField;
    private JTextField roomNoField;
    private JTextField dateField;
    private JButton reserveRoomButton;
    private JLabel successLabel;
    private JButton cancelReservationButton;

    public ReserveRoomWindow(LibraryDB libraryDB) {
        setContentPane(contentPane);
        setModal(true);
        setTitle("Reserve Room");
        WindowUtils.centerAndPack(this);
        setResizable(false);

        reserveRoomButton.addActionListener(e -> {
            String id = idField.getText();
            String room = roomNoField.getText();
            String dateString = dateField.getText();

            if (id.isEmpty() || room.isEmpty() || dateString.isEmpty()) {
                JFrame frame = new JFrame("ErrorDialog");
                JOptionPane.showMessageDialog(frame,
                                              "Please include values for all fields.",
                                              "SQL Exception",
                                              JOptionPane.ERROR_MESSAGE);
            }
            else {
                DateTimeStringConverter converter = new DateTimeStringConverter(new SimpleDateFormat("yyyy-MM-dd"));
                Timestamp date = new Timestamp(converter.fromString(dateString).getTime());

                if (libraryDB.getReservationSystem().reserveRoom(Integer.parseInt(id), Integer.parseInt(room), date)) {
                    successLabel.setText("Successfully reserved room " + room + " on " + dateString + ".");
                    idField.setText("");
                    roomNoField.setText("");
                    dateField.setText("");
                }
                else {
                    successLabel.setText("Failed to reserve room " + room + ".");
                }
            }
        });
        cancelReservationButton.addActionListener(e -> {
            String id = idField.getText();
            String room = roomNoField.getText();
            String dateString = dateField.getText();

            if (id.isEmpty() || room.isEmpty() || dateString.isEmpty()) {
                JFrame frame = new JFrame("ErrorDialog");
                JOptionPane.showMessageDialog(frame,
                                              "Please include values for all fields.",
                                              "SQL Exception",
                                              JOptionPane.ERROR_MESSAGE);
            }
            else {
                DateTimeStringConverter converter = new DateTimeStringConverter(new SimpleDateFormat("yyyy-MM-dd"));
                Timestamp date = new Timestamp(converter.fromString(dateString).getTime());

                libraryDB.getReservationSystem().deleteReservation(Integer.parseInt(id), Integer.parseInt(room), date);
                idField.setText("");
                roomNoField.setText("");
                dateField.setText("");
            }
        });
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        contentPane = new JPanel();
        contentPane.setLayout(new GridLayoutManager(6, 4, new Insets(10, 10, 10, 10), -1, -1));
        final JLabel label1 = new JLabel();
        label1.setText("Cardholder ID:");
        contentPane.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        idField = new JTextField();
        contentPane.add(idField, new GridConstraints(0, 1, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Room Number:");
        contentPane.add(label2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        roomNoField = new JTextField();
        contentPane.add(roomNoField, new GridConstraints(1, 1, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final JLabel label3 = new JLabel();
        label3.setText("Date:");
        contentPane.add(label3, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        dateField = new JTextField();
        contentPane.add(dateField, new GridConstraints(3, 1, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        reserveRoomButton = new JButton();
        reserveRoomButton.setText("Reserve Room");
        contentPane.add(reserveRoomButton, new GridConstraints(4, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        contentPane.add(spacer1, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        successLabel = new JLabel();
        successLabel.setText(" ");
        contentPane.add(successLabel, new GridConstraints(5, 1, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        label4.setText("Date in YYYY-MM-DD format:");
        contentPane.add(label4, new GridConstraints(2, 1, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        cancelReservationButton = new JButton();
        cancelReservationButton.setText("Cancel Reservation");
        contentPane.add(cancelReservationButton, new GridConstraints(4, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /** @noinspection ALL */
    public JComponent $$$getRootComponent$$$() {
        return contentPane;
    }
}
