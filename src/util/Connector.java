package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Connector {

    private static int loginAttempts;

    private Connector() {
    }

    public static Connection connect(String username, String password) {

        try {
            Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//cs304.c74bwsyjbcgy.us-west-2.rds.amazonaws.com:1521/ORCL", username, password);
            System.out.println("\nConnected to Oracle!");
            return con;
        } catch (SQLException ex) {
        	JFrame frame = new JFrame("ErrorDialog");
        	JOptionPane.showMessageDialog(frame,
        		    ex.getMessage(),
        		    "SQL Exception",
        		    JOptionPane.ERROR_MESSAGE);
            return null;
        }
    }

    public static int getLoginAttempts() {
        return loginAttempts;
    }

    public static void addLoginAttempt() {
        loginAttempts++;
    }
}
