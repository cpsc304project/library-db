package util;

import java.awt.*;

public class WindowUtils {

    private WindowUtils() {
    }

    public static void centerAndPack(Window window) {
        window.pack();
        Dimension d = window.getToolkit().getScreenSize();
        Rectangle r = window.getBounds();
        window.setLocation((d.width - r.width) / 2, (d.height - r.height) / 2);
    }
}
